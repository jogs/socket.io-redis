'use strict';
/*
** Usar 'use strict' e introducir el codido en un closure es un buena practica
*/
(function(){
	/*
	** Definiendo la instancia de express
	*/
	var express = require('express');
	/*
	** La variable 'app' es una instancia de 'express'
	*/
	var app = require('express')();
	/*
	** La variable 'server' es una instancia de 'http' bajo 'express'
	*/
	var server = require('http').createServer(app);
	/*
	** La variable 'io' es una instancia de 'socket.io' bajo la variable 'server'
	*/
	var io = require('socket.io')(server);
	/*
	** La variable 'port' toma el valor del puerto predefinido por el entorno del servidor,
	** si no esta definido toma el valor 3030
	*/
	var port = process.env.PORT || 3030;
	/*
	** La variable 'redis' es una instancia de 'redis-node'
	*/
	var redis = require("redis");
	/*
	** La variable 'client' es una instancia del cliente que provee 'redis-node'
	*/
	var client = redis.createClient();
	/*
	** se inicia el cliente redis y si existe un error sera lanzado a consola
	*/
	client.on("error", function (err) {
	  console.log("Error " + err);
	});
	/*
	** La variable 'client' es una instancia del cliente que provee 'redis-node'
	*/
	var watcher = redis.createClient();
	var messages = [{
		author: "Server",
		text: "Bienvenido, el servicio inicio a "+Date()
	}];
	/*
	** se inicia el cliente redis y si existe un error sera lanzado a consola
	*/
	watcher.on("error", function (err) {
	  console.log("Error " + err);
	});
	/*
	** Definir el directorio de entrada public
	*/
	app.use(express.static('public'));

	app.get('/', function(req, res) {  
	  res.status(200).sendFile("index.html");
	});

	watcher.on("subscribe", function (channel, count) {
		console.log("Servidor suscrito al canal "+channel);
		/*
		** se inicia io
		*/
		io.on('connection', function(socket){
		  console.log("Nuevo Cliente conectado via WebSockets");
		  socket.emit('messages', messages);
		  socket.on('newMessage', function(data) {
		  	console.log('Nuevo Mensaje de '+data.author);
		    client.publish('redisChat','{"author":"'+data.author+'","text":"'+data.text+'"}');
		  });
		  socket.on('disconnect', function(){
		  	console.log('Se a desconectado un cliente');
		  });
		});
	});

	watcher.on("message", function (channel, message) {
    console.log(channel + ": "+message);
    messages.push(JSON.parse(message));
    io.sockets.emit('messages', JSON.parse(message));
	});
 
  watcher.subscribe('redisChat');
	server.listen(port, function () {
	  console.log('Server listening at port %d', port);
	});
})();