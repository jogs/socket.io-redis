var socket = io.connect('http://localhost:3030', { 'forceNew': true });

var messages = [];

socket.on('messages', function(data) {  
  if (messages.length===0) {
   messages=data;
  } else {
   messages.push(data); 
  }
  console.log(messages);
  render(messages);
})

function render (data) {  
  var html = data.map(function(elem, index) {
    return(`<div>
              <strong>${elem.author}</strong>:
              <em>${elem.text}</em>
            </div>`);
  }).join(" ");

  document.getElementById('messages').innerHTML = html;
}

function addMessage(e) {  
  var message = {
    author: document.getElementById('username').value,
    text: document.getElementById('texto').value
  };
  document.getElementById('username').value = '';
  document.getElementById('texto').value = '';
  socket.emit('newMessage', message);
  return false;
}